package com.epam.menu.view;

import java.util.Scanner;

public enum MenuEnum {
    TEST(()->{ testCommand(); }),
    SECOND(()->{ secondTest(); }),
    EXIT(()->{ exit();
    });

    private static void outputMenu() {
        System.out.println("\nMENU");
        int i=1;
        for (MenuEnum menuEnum : MenuEnum.values()) {
            System.out.println(""+ i++ +" - "+menuEnum);
        }
    }
    private Printable printable;
    public static void show(){
        Scanner scanner = new Scanner(System.in);
        String keyMenu;
        do{
            outputMenu();
            System.out.println("Please select menu point");
            keyMenu=scanner.nextLine().toUpperCase();
            try{
                MenuEnum.values()[Integer.parseInt(keyMenu)-1].printable.print();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }while(!keyMenu.equals("3"));
    }

    MenuEnum(Printable printable) {
        this.printable=printable;
    }

    private static void testCommand() {
        System.out.println("testCommand");
    }

    private static void secondTest() {
        System.out.println("Second test");
    }

    private static void exit() {
        System.out.println("exit");
    }
}
