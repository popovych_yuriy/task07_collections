package com.epam.menu.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class MenuMap {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MenuMap.class);

    public MenuMap() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Test menu");
        menu.put("2", "  2 - Second test");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        logger.info("Test menu");
    }

    private void pressButton2() {
        logger.info("Second test");
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
