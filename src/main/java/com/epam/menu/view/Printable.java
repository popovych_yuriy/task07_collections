package com.epam.menu.view;

@FunctionalInterface
public interface Printable {

    void print();
}
