package com.epam.binarytree.model;

import java.util.*;

public class MyBinTree <K extends Comparable, V> implements Map<K, V> {
    private int size = 0;
    private Node<K, V> root;

    private static class Node<K, V> implements Map.Entry<K, V>{
        private Node<K, V> left;
        private Node<K, V> right;
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return null;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0 ? true : false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while(node != null){
            int cmp = k.compareTo(node.key);
            if(cmp < 0){
                node = node.left;
            }else if(cmp > 0){
                node = node.right;
            }else{
                return node.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> parent = root;
        Node<K, V> tempParent = root;
        if(root == null){
            root = new Node<>(key, value);
        }else{
            int cmp;
            do{
                parent = tempParent;
                cmp = key.compareTo(tempParent.key);
                if(cmp < 0){
                    tempParent = tempParent.left;
                }else  if(cmp > 0 ){
                    tempParent = tempParent.right;
                }else{
                    V oldValue = tempParent.value;
                    tempParent.value = value;
                    return oldValue;
                }
            }while (tempParent != null);

            if(cmp < 0){
                parent.left = new Node<>(key, value);
            }else if(cmp > 0){
                parent.right = new Node<>(key, value);
            }
        }
        size++;
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        Set<K> set = new HashSet<>();
        runTreeForKey(root, set);
        return set;
    }

    private void runTreeForKey(Node<K,V> localRoot, Set<K> set) {
        if(localRoot != null){
            runTreeForKey(localRoot.left, set);
            set.add(localRoot.getKey());
            runTreeForKey(localRoot.right, set);
        }
    }

    @Override
    public Collection<V> values() {
        Collection<V> array = new ArrayList<>();
        runTreeForValue(root, array);
        return array;
    }

    private void runTreeForValue(Node<K,V> localRoot, Collection<V> array) {
        if(localRoot != null){
            runTreeForValue(localRoot.left, array);
            array.add(localRoot.getValue());
            runTreeForValue(localRoot.right, array);
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTreeForNode(root, set);
        return set;
    }

    private void runTreeForNode(Node<K, V> localRoot, Set<Entry<K, V>> set){
        if(localRoot != null){
            runTreeForNode(localRoot.left, set);
            set.add(localRoot);
            runTreeForNode(localRoot.right, set);
        }
    }
}
