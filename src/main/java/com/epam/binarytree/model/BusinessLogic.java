package com.epam.binarytree.model;

public class BusinessLogic implements Model {
    private MyBinTree<String, Integer> myBinTree;
    public BusinessLogic() {
        myBinTree = new MyBinTree<>();
    }

    public MyBinTree getMyBinTree() {
        return myBinTree;
    }

    public void setMyBinTree() {
        myBinTree.put("Alpha",647);
        myBinTree.put("Beta",453);
        myBinTree.put("Gamma",449);
        myBinTree.put("Delta",657);
        myBinTree.put("Epsilon",765);
        this.myBinTree = myBinTree;
    }
}
