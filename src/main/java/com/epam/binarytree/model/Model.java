package com.epam.binarytree.model;

public interface Model {
    void setMyBinTree();
    MyBinTree getMyBinTree();
}
