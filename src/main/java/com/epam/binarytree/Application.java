package com.epam.binarytree;

import com.epam.binarytree.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
