package com.epam.binarytree.view;

@FunctionalInterface
public interface Printable {

    void print();
}
