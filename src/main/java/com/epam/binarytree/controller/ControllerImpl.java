package com.epam.binarytree.controller;

import com.epam.binarytree.model.BusinessLogic;
import com.epam.binarytree.model.Model;
import com.epam.binarytree.model.MyBinTree;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void setMyBinTree() {
        model.setMyBinTree();
    }

    @Override
    public MyBinTree getMyBinTree() {
        return model.getMyBinTree();
    }
}