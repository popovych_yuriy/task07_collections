package com.epam.binarytree.controller;

import com.epam.binarytree.model.MyBinTree;

public interface Controller {
    void setMyBinTree();
    MyBinTree getMyBinTree();
}
